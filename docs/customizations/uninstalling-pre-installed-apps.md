---
title: Uninstalling pre-installed apps (safe method)
---

### By [Speeduploop](https://groups.google.com/d/msg/bananahackers/A9ATI7q1QJk/SO8vp2fmAwAJ)

Now that we have [temporary root](../root/temporary-root.md) tools we can uninstall
pre-installed apps without loosing OTA-updates.

All the apps are installed on the /data partition, even the pre-installed 
(the app-copies on /system are only the "installation-media").

<iframe width="507" height="285" src="https://www.youtube.com/embed/4Td_uCIWCLo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

So the simplest way to get rid of unwanted apps is:

1. Get a [temporary root access](../root/temporary-root.md). Remember that the 
following commands are written for when you use Telnetd app (like in the previous
video), but you can get root in an easier way using Wallace and ADBRoot. Using
them you don't need to write `busybox` or `busybox telnet localhost` and write 
`exit` just one time.

2. Get the webapps.json file from the /data partition
```
adb shell
busybox telnet localhsot
cp /data/local/webapps/webapps.json /sdcard/
exit
exit
```

3. Open webapps.json with a text editor which preserve the line endings(on Windows
you can use Notepad++). For every app which you might want to uninstall, change the
field/line `"removable": false,` to `"removable": true,`.

4. Push webapps.json to "/data/local/tmp". To have a modded copy of webapps.json on devices's
temp folder:
```
adb push webapps.json /data/local/tmp
```
And to replace the **real** webapps.json:
```
adb shell
busybox telnet localhost
cd /data/local/webapps
cp webapps.json webapps.json_bak
rm webapps.json && cp /data/local/tmp/webapps.json . && chmod 600 webapps.json && chown root:root webapps.json
```
5. Now check permissions: `ls -la webapps.json` and it should respond with something
like this:
```
-rw------- root root 57563 2019-03-23 15:58 webapps.json
```
6. If everything's fine do `adb reboot`. After reboot the apps are still there, 
but the "options" menu of the app drawer now allows you to uninstall them in a 
clean way like any other app! Because we did not modify the /system partition:

 - OTA updates are not prevented
 - pre-installed apps will re-install on a factory reset.
 
But until factory reset the apps are gone and the space on /data is freed.