---
title: Install OmniSD <small>or any other third-party appliacation on your KaiOS device!</small>
---

[Install OmniSD on KaiOS - all YouTube videos](https://www.youtube.com/playlist?list=PLrmJqZiRHC9OVI_ZIz0K4GPf73hsJGw4j)

### How to install any app on KaiOS / Firefox OS normally

The sideload is the process of transferring files between two local devices, in particular between a computer and a mobile device.

On Firefox OS it was a natural property, and it can be done using 
[ADB](../development/adb) and [DevTools](../development/webide) also on [more KaiOS devices](../devices/) ([source]()).
The installation of OmniSD is a classic example.

Many devices can sideload third-party apps, but other devices require special
patches ([check the list](../devices/)).

Let's start by seeing how to sideload naturally, before moving on to jailbreak.

<iframe width="507" height="285" src="https://www.youtube.com/embed/IVKY-XY0Hac" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

##### Requirements

1. [DEBUG MODE](../development/debug-mode), your [device](../devices/) must be able to do this. Normally it is sufficient to dial the debug code *#*#33284#*#*, while on some devices (in addition) it is necessary to open a menu using the code \*#\*#0574#\*#\* and from there enable the USB debugging;
2. [ADB (Android Debug Bridge)](../development/adb), a versatile command-line tool that allows you to communicate with a device and that facilitates a variety of device actions;
3. [WEBIDE](../development/webide), the most common and complete of development tools. Choose an old version of Firefox (59 or earlier) or Pale Moon (28.6.1 or earlier), or use the official Kaios emulator (KaiOSRT). Alternatively you can use other command line development tools like [Gdeploy](https://gitlab.com/suborg/gdeploy), that uses NodeJS, or [XPCshell](https://github.com/jkelol111/make-kaios-install).

##### Proceedings

1. Enable the debug mode on your device;
2. Connect the device to the PC using a USB cable;
3. Open WebIDE and connect to the "Remote runtime" (this should work on the official Kaiostr emulator), if not seen, start the `adb forward tcp:6000 localfilesystem:/data/local/debugger-socket` command and click again on "Remote runtime". If an error message about build date mismatch appears, you can safely ignore it. If the connection doesn't work, try rebooting the phone, running the `adb forward` command and connecting again.
4. Select the application's folder in the "Open packaged app" button of WebIDE.
5. With the triangular "Play button" at the top center of WebIDE, the app will be installed on your phone.

#### Difference between "normal sideload" and "Jailbreak"

The official jailbreak method differs from the normal sideload only in the fact that, after point 5, a "privileged factory reset" is required directly from the jailbreak application that we will install (OmniSD, [Wallace or Wallace-Toolbox](.,/root/temporary-root)) by pressing the # key from it, in order to automatically activate all the "Developer" options. After the reset, you will need to repeat the whole procedure if you want to use these applications for a different use (sideload without PC or root permissions).

### What is OmniSD and why it is so important

OmniSD is a third-party application for KaiOS, [developed by Luxferre in August 2018](http://omnijb.831337.xyz/), and like other applications it is written in HTML, JavaScript and CSS. The weight of the installation is approximately 58 kilobytes. You can download the standalone OmniSD package from [here](https://groups.google.com/group/bananahackers/attach/21a2c79eb6524/OmniSD.zip?part=0.2&authuser=0) (alternative links [here](https://cloud.disroot.org/s/QTC5oM4tZ9rWpAZ) or [here](https://omnijb.831337.xyz/OmniSD.zip)).

OmniSD detects packaged applications in zip format present in the "downloads" or the "apps" folder of the sd card or the internal memory to then install them ([jailbreak](official-jailbreak)).

Another feature of this application is the ability to get a "Privileged Factory Reset" by pressing the # key while the app is running. This enable the "Developer" menu within the "Settings" application on the system, allowing the user to access [ADB](../development/adb) and [DevTools](../development/webide) directly from the menu.

<iframe width="507" height="285" src="https://www.youtube.com/embed/uOiJLyz9T2Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Difference between "Jailbreak" and "Root"

 - **Jailbreaking** is a process that enables the development menu and allows installing third-party apps.
 - **Rooting** is a process that allows root-level of [ADB](../development/adb) console access (for more info visit the "[ROOT](../root/)" section).

#### Privileged mode

The privileged mode, as the word itself says, is a boot mode in which the user has full control over the development tools and hidden components of the device, such as the "Developer" menu in the "Settings" app, you can also debug on the pre-installed applications and access the "Device Preferences" from WebIDE.

![WebIDE normal mode](../../images/webide_normal_mode.png)
![WebIDE privileged mode](../../images/webide_privileged_mode.png)

This mode still allows you to obtain updates, and can be removed simply by performing a normal factory reset from the Settings app or from Recovery mode.

#### How to enable the "Developer" menu on KaiOS

The "Developer" menu allows you to [debug on a device](../development/debug-mode), 
in addition to accessing [ADB](../development/adb) and [DevTools](../development/webide), as well as 
various other utilities that can be consulted [here](https://developer.mozilla.org/en-US/docs/Archive/B2G_OS/Debugging/Developer_settings). It 
is hidden inside the "Settings" app, and is visible only 
when the device is in "privileged mode". Learn more here: [Developer menu](../development/developer-menu)

### Safe jailbreak methods

The safe method consists in working exclusively on the data partition, which is the only dynamic partition, subject to various changes and events due to the use of the user from the first start. If this partition is damaged, simply perform a factory reset to restore everything (when you perform "Wipe data / factory reset" from Recovery Mode you're cleaning the data partition):

 - [Official jailbreak](official-jailbreak): Performing a normal app sideload for OmniSD
 - [Mediatek jailbreak](mediatek-jailbreak): This method requires Python3 and Fastboot installed
 - [Mannual jailbreak](enable-adb-devtools): Manually enable ADB & DevTools on the /data partition
 - [Manual /data patch](): It consists in positioning OmniSD on the /data partition

Note that [not all KaiOS devices have the same possibilities](../devices/). To help users better understand which installation method is more suitable, have classified KaiOS devices. Learn more [here](../devices/).

#### What is the app format accepted by OmniSD?

OmniSD accepts app packages in the .zip format. An archive must contain three files:

1. **application.zip** file with the actual WebIDE compatible KaiOS/FFOS app
2. **update.webapp** file which can be empty but __must__ be present
3. **metadata.json** file in the following format:
```
{"version": 1, "manifestURL": "app://[your_app_id]/manifest.webapp"},
```
Where `[your_app_id]` has to be replaced with the actual ID (origin) of your app registered in the manifest, and `manifest.webapp` has to be renamed if it's called differently in the `application.zip` archive. Other than that, the application structure in `application.zip` must match the general KaiOS and Firefox OS app structure. Learn more here: [Your first app](../development/your-first-app)

#### Other applications similar to OmniSD

OmniSD is an application developed by Luxferre in the summer of 2018, when KaiOS debuted in Europe for the first time, thanks to Nokia 8110 4G the bananaphone. Luxferre has created various projects, including a custom privacy-oriented rom (GerdaOS) and a real library of extensions and various tools for KaiOS. Here are some applications developed by him, containing some features in common with OmniSD:

 - [GerdaOS File Manager](gerda-filespkggab), a modified version of the stock File Manager for KaiOS, that installs omnisd packages by not limiting itself to the "download" and "app" folders, unlike OmniSD;
 - [Wallace](../root/temporary-root) is an application that enables root privileges and can perform "privileged reset". A classic version, a light version (without adbd binary file) and an optimized version for devices with Spreadtrum chipset are available for this app;
 - [Wallace-Toolbox](../root/temporary-root) (see the "Temporary Root" page) is the evolution of Wallace, which includes the same functions and various tools, such as the call recorder (KaiOS 2.5.2) and an IMEI repairer.
 - [Perry's bhacker store](https://github.com/strukturart/kaiOs-alt-app-store) which grabs apps from internet and installs them on your device. It does not install apps from your phone but from remote hosts. Currently it has got a limited number of apps.


### Extreme and unsafe jailbreak method

[Extreme jailbreak](extreme-jailbreak), also known as Omnijb-final.zip or Smith.zip , the extreme method means stopping updates permanently. This guide is obsolete, not recommended and for educational purposes only! Use at your own risk!