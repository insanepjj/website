---
title: Documentation
toc: false
---
# Docs / Wiki

Welcome to the Bananachakers wiki, a source of knowlegde about kaiOS and some Guides for modding KaiOS to make it fit your needs.

On the side you can see overview of all pages, if you are on a small screen you can find the menu on top of this page.