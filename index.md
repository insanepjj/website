---
layout: home
---

Banana Hackers
===============
The very first community dedicated to the secrets of KaiOS!

[JOIN THE REVOLUTION!](docs/devices/)

<style>
div.showcase-box {
    display:flex;
    flex-wrap: wrap;
    justify-content:space-evenly;
}
.showcase-box>a{
    padding:4px;
    display:flex;
    flex-direction:column;
    align-items:center;
}
.showcase-box>a>img{
    width: 80px
}
.showcase-box>a>div{
    font-size:15px;
    font-weight: bold;
}
</style>


## Wiki sections

<div class="showcase-box">
    <a href="docs/devices">
        <img src="./images/devices.gif"/>
        <div>KaiOS DEVICES</div>
    </a>
    <a href="docs/install-omnisd">
        <img src="./images/install_omnisd.gif"/>
        <div>Install OmniSD</div>
    </a>
    <a href="#">
        <img src="./images/backup.gif"/>
        <div>BACKUP/REPAIR</div>
    </a>
    <a href="#">
        <img src="./images/matrix.gif"/>
        <div>ROOT</div>
    </a>
    <a href="./docs/development/index.md">
        <img src="./images/development.gif"/>
        <div>DEVELOPMENT</div>
    </a>
    <a href="#">
        <img src="./images/gerda.gif"/>
        <div>CUSTOMIZATIONS</div>
    </a>
</div>

## Community

<div class="showcase-box">
    <a href="https://groups.google.com/forum/#!forum/bananahackers">
        <img src="./images/google_groups.png"/>
        <div>Google Groups Forum</div>
    </a>
    <a href="https://www.youtube.com/channel/UCwe7iCNm4ZJpP9wN_Y02kbA/community?app=desktop&persist_app=1">
        <img src="./images/youtube.png"/>
        <div>Youtube</div>
    </a>
    <a href="https://www.google.com/url?q=https%3A%2F%2Fwww.facebook.com%2FBananaHackers-101263954558113&sa=D&sntz=1&usg=AFQjCNEfO6gjzMYITEafBIM4len_F2b25g">
        <img src="./images/facebook.png"/>
        <div>Facebook</div>
    </a>
    <a href="https://twitter.com/bananahackers">
        <img src="./images/twitter.png"/>
        <div>Twitter</div>
    </a>
    <a href="https://discordapp.com/invite/rQ93zEu">
        <img src="./images/discord.png"/>
        <div>Discord</div>
    </a>
    <a href="mailto:bananahackers@googlegroups.com">
        <img src="./images/email.png"/>
        <div>Email Mailing List</div>
    </a>
</div>

## External links
The most useful links for external sites, directly on the front page for you.

- [B-Hackers Store](https://sites.google.com/view/b-hackers-store) - download free apps here
- [edl.bananahackers.net](https://edl.bananahackers.net) - Firehose loaders collection
- [Gerda.Tech](https://gerda.tech) - Custom rom for Nokia 8110
- [KaiOS ITALIA](https://sites.google.com/view/kaiositalia) - Italian clone of this site



>*“The Matrix is a system, Neo. That system is our enemy. But when you're inside, you look around, what do you see? Businessmen, teachers, lawyers, carpenters. The very minds of the people we are trying to save. But until we do, these people are still a part of that system and that makes them our enemy. You have to understand, most of these people are not ready to be unplugged. And many of them are so inured, so hopelessly dependent on the system, that they will fight to protect it.”*
>
> Morpheus, The Matrix (1999)
